package com.codigomortalito.tdd_demo

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
internal class CreatePromoAcceptanceTests {

    @Autowired
    private lateinit var testRestTemplate: TestRestTemplate

    @Test
    fun shouldCreatePromo() {
        // given
        val creationRequest = aPromocodeCreationRequest()

        // when
        val creationResponse = testRestTemplate.postForEntity(
            "/api/promo-code",
            jsonRequest(serializeToJson(creationRequest)),
            PromocodeResponse::class.java
        )

        //then
        assertThat(creationResponse.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(creationResponse.body).apply {
            extracting("discount").isEqualTo(creationRequest.discount)
        }
    }

    private fun jsonRequest(bodyContentInJson: String): HttpEntity<String> {
        return HttpEntity<String>(
            bodyContentInJson,
            HttpHeaders().apply { contentType = MediaType.APPLICATION_JSON }
        )
    }

    private fun serializeToJson(anyObject: Any): String {
        return ObjectMapper().registerModule(KotlinModule()).writeValueAsString(anyObject)
    }

    private fun aPromocodeCreationRequest(): PromocodeCreationRequest {
        return PromocodeCreationRequest(25.00)
    }
}