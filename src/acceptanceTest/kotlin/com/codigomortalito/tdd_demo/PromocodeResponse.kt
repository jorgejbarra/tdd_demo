package com.codigomortalito.tdd_demo

data class PromocodeResponse(val id: String, val code: String, val discount: Double)
