package com.codigomortalito.tdd_demo.controller

data class PromoCodeResponse(val id: String, val discount: Double, val code: String)
