package com.codigomortalito.tdd_demo.controller

import arrow.core.Either
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.util.*
import javax.validation.Valid

@RestController
class CreatePromocodeController {

    @PostMapping("/api/promo-code")
    fun createPromocode(@RequestBody @Valid request: CreatePromocodeRequest): ResponseEntity<PromoCodeResponse> {
        val f: ResponseEntity<PromoCodeResponse> = ResponseEntity.badRequest().build()
        return callService(request).fold(
            { ResponseEntity.ok(PromoCodeResponse(UUID.randomUUID().toString(), request.discount!!, "LK-23023")) },
            { f }
        )
    }

    private fun callService(request: CreatePromocodeRequest): Either<PromoCodeCreationError, PromoCode> {
        TODO("Not yet implemented")
    }
}

sealed class PromoCodeCreationError

data class PromoCode(val id: String, val discount: Double, val code: String)
