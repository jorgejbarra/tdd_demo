package com.codigomortalito.tdd_demo.controller

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotNull

data class CreatePromocodeRequest(@field:JsonProperty("") @field:NotNull var discount: Double?)
