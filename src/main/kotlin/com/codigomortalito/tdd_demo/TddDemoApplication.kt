package com.codigomortalito.tdd_demo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TddDemoApplication

fun main(args: Array<String>) {
    runApplication<TddDemoApplication>(*args)
}
