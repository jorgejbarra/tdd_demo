# Promocode API

- Create promocode

`[Post] promocode`

```json
{
  "discount": 15.00
}
```

### Run rabbit from console

``
docker run --rm -it --hostname my-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management
``